package controls;

import java.util.ArrayList;

public class Cluster {
	private ArrayList<Integer> elements;
	
	public Cluster(int root) {
		elements = new ArrayList<Integer>();
		elements.add(root);
	}
	
	public static ArrayList<Cluster> getClusters(DirectedGraph<Location> graph) {
		ArrayList<Edge> edges = graph.edges();
		ArrayList<Cluster> clusters = new ArrayList<Cluster>();
	
		int[] vertex = new int[graph.getVertexCount()];
		for (int i = 0; i < vertex.length; i++)
			vertex[i] = i;
		
		for (Edge edge: edges) {
			vertex[edge.getTo()] = edge.getFrom();
		}
		
		int root;
		for (int i = 0; i < vertex.length; i++) {
			root = clusterRoot(vertex, i);
			addVertexToCluster(clusters, root, i);
		}	
		return clusters;
	}
	
	private static void addVertexToCluster(ArrayList<Cluster> clusters, int root, int i) {
		boolean vertexAdded = false;
		for (Cluster cluster: clusters) {
			if (cluster.size() == 0)
				continue;
			if (cluster.get(0) != root)
				continue;
			
			cluster.add(i);
			vertexAdded = true;
		}
		if (!vertexAdded) {
			Cluster cluster = new Cluster(root);
			if (root != i)
				cluster.add(i);
			clusters.add(cluster);
		}	
	}

	private static boolean isClusterRoot(int[] vertex, int i) {
		if (i < 0 || i >= vertex.length)
			throw new IllegalArgumentException("Indice de v�rtice inv�ldo");
		
		return (vertex[i] == i);
	}

	private static int clusterRoot(int[] vertex, int i) {
		return (vertex[i] == i)? i : clusterRoot(vertex, vertex[i]);
	}

	
	public int get(int index) {
		validateIndex(index);
		return elements.get(index);
	}
	
	private void validateIndex(int index) {
		if (index < 0 || index >= size()) {
			throw new IllegalArgumentException("�ndice inv�lido");
		}
	}

	public void add(int element) {
		if (exists(element))
			return;
		elements.add(element);
	}

	public boolean exists(int element) {
		return elements.contains(element);
	}
	
	public int size() {
		return elements.size();
	}
	
	public ArrayList<Integer> getElements(){
		return elements;
	}
}