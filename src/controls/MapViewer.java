package controls;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.Layer;
import org.openstreetmap.gui.jmapviewer.MapMarkerCircle;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.MapRectangleImpl;
import org.openstreetmap.gui.jmapviewer.Style;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;
import org.openstreetmap.gui.jmapviewer.interfaces.MapRectangle;
import org.openstreetmap.gui.jmapviewer.interfaces.TileCache;

public class MapViewer extends JMapViewer {
	
	public static final int MAX_COLOR =  16777216;
	private static final double BASIC_ADJUSTMENT = 3;

	public MapViewer() {super();}
	public MapViewer(TileCache tileCache) {super(tileCache);}

	public void addPoints(ArrayList<Location> locations) {		
		if (locations.size() == 0)
			return;		
		int count = 0;
		for (Location location : locations) {	
			//drawMarker(count++ + "", new Coordinate(location.getLatitude(), location.getLongitude()));
			drawMarker("", location);
		}		
		setDisplayToFitMapMarkers();
	}
	public void findClusters(DirectedGraph<Location> graph, double adjustment) {
		if (graph.edges().size() >= graph.getVertexCount())
			graph = graph.getMST();
		
		ArrayList<Edge> edges = graph.edges();	
		double averageWeight=0;
		
		for (Edge edge: edges) {
			averageWeight += edge.getWeight();
		}		
		averageWeight /= edges.size();
		averageWeight *= BASIC_ADJUSTMENT;
		if (adjustment > 0)
			averageWeight *= adjustment;
		
		edges.sort(null);
		for (Edge edge: edges) {
			if (edge.getWeight() > averageWeight) {
				graph.removeEdge(edge.getFrom(), edge.getTo());
			}				
		}		
	}
	public void showClusters(DirectedGraph<Location> graph) {
		ArrayList<Cluster> clusters = Cluster.getClusters(graph);
		List<MapMarker> markerList = getMapMarkerList();
		Color color;
		Style style;
		//removeAll();
		int clusterIndex = 0;
		for (Cluster cluster: clusters) {
			color = getClusterColor(clusterIndex);
			for (Integer index: cluster.getElements()) {
				style = markerList.get(index).getStyle();
				style.setBackColor(color);
			}
			clusterIndex++;
		}
		repaint();
	}
	
	public void hideClusters() {
		for (MapMarker mapMarker: getMapMarkerList()) {
			mapMarker.getStyle().setBackColor(Color.YELLOW);
		}
		repaint();
	}
	
	public void drawLine(Location location1, Location location2) {
		Coordinate coord1 = new Coordinate(location1.getLatitude(), location1.getLongitude());
		Coordinate coord2 = new Coordinate(location2.getLatitude(), location2.getLongitude());
		addMapPolygon(new MapPolygonImpl(coord1, coord2, coord1));
	}

	public void drawCircle(Location location, double radius) {
		Coordinate coord = new Coordinate(location.getLatitude(), location.getLongitude());
		addMapMarker(new MapMarkerCircle(coord, radius)); // 0.0001 se ve bien
	}
	
	public void setGraph(DirectedGraph graph) {
		ArrayList<Edge> edges = graph.edges();
		List<MapMarker> markers =  getMapMarkerList();
		removeAllMapPolygons();
		Coordinate coord1;
		Coordinate coord2;
		for (Edge edge: edges) {
			coord1 = markers.get(edge.getFrom()).getCoordinate();
			coord2 = markers.get(edge.getTo()).getCoordinate();
			drawLine(coord1, coord2);
		}
	}

	public void drawMarker(String name, Location location) {
		addMapMarker(new MapMarkerDot(name, new Coordinate(location.getLatitude(), location.getLongitude())));
	}
	
	public void drawLine(Coordinate coord1, Coordinate coord2) {
		addMapPolygon(new MapPolygonImpl(coord1, coord2, coord1));
	}
	
	private Color getClusterColor(int clusterIndex) {
		Color[] res = new Color[18];
		res[0] = new Color(255, 0, 0);
		res[1] = new Color(0, 255, 0);
		res[2] = new Color(0, 0, 255);
		res[3] = new Color(255, 0, 255);
		res[4] = new Color(0, 255, 255);
		res[5] = new Color(255, 128, 0);
		res[6] = new Color(255, 0, 128);
		res[7] = new Color(255, 128, 128);
		res[8] = new Color(128, 0, 255);
		res[0] = new Color(0, 128, 255);
		res[10] = new Color(128, 128, 255);
		res[11] = new Color(128, 255, 0);
		res[12] = new Color(0, 255, 128);
		res[13] = new Color(128, 0, 0);
		res[14] = new Color(0, 128, 0);
		res[15] = new Color(0, 0, 128);
		res[16] = new Color(128, 0, 128);
		res[17] = new Color(0, 128, 128);
		
		return res[clusterIndex %  res.length];
	}

	public int getItemMarkerAt(Point point) {
        for( MapMarker marker : getMapMarkerList()){
            Point lMarkerPoint = getMapPosition(marker.getLat(), marker.getLon());
            if( point.distance(lMarkerPoint) > marker.getRadius())
            	continue;
            return getMapMarkerList().indexOf(marker);
        }
        return -1;
	}
	
	public void setMarkerPosition(int markerItem, Point point) {
		validateMarkerItem(markerItem);
		
		double latitude = getPosition(point).getLat();
		double longitude = getPosition(point).getLon();
		getMapMarkerList().get(markerItem).setLat(latitude);
		getMapMarkerList().get(markerItem).setLon(longitude);
	}
	
	public void highlightMarker(int markerItem, Color color) {
		validateMarkerItem(markerItem);
		getMapMarkerList().get(markerItem).getStyle().setColor(color);	
		repaint();
	}
	
	public void highlightMarkers(int[] markerItems, Color color) {
		for (int item: markerItems) {
			highlightMarker(item, color);
		}
	}

	public void unHighlightAllMarkers() {
		for (MapMarker marker: getMapMarkerList()) {
			marker.getStyle().setColor(Color.BLACK);
		}
		repaint();
	}

	public int[] getItemsMarkerFromSelection(Point p1, Point p2) {
		Stack<Integer> markersItems = new Stack<Integer>();
		Point auxP1 = new Point((p1.x < p2.x)?p1.x:p2.x, (p1.y < p2.y)?p1.y:p2.y);
		Point auxP2 = new Point((p1.x > p2.x)?p1.x:p2.x, (p1.y > p2.y)?p1.y:p2.y);
		Point markerPoint;
		for (MapMarker marker : getMapMarkerList()) {
			markerPoint = getMapPosition(marker.getLat(), marker.getLon());
			if (markerPoint.x > auxP1.x && markerPoint.x < auxP2.x
					&& markerPoint.y > auxP1.y && markerPoint.y < auxP2.y) {
				markersItems.push(getMapMarkerList().indexOf(marker));				
			}
		}
		int[] ret = new int[markersItems.size()];
		int index = 0;
		while (!markersItems.empty()) {
			ret[index++] = markersItems.pop();
		}
		return ret;
	}

	public void drawRectangle(Point p1, Point p2, Color color) {
		Point auxP1 = new Point((p1.x < p2.x)?p1.x:p2.x, (p1.y < p2.y)?p1.y:p2.y);
		Point auxP2 = new Point((p1.x > p2.x)?p1.x:p2.x, (p1.y > p2.y)?p1.y:p2.y);
		MapRectangle rect = new MapRectangleImpl(
				new Coordinate(getPosition(auxP1).getLat(), getPosition(auxP1).getLon()),
				new Coordinate(getPosition(auxP2).getLat(),	getPosition(auxP2).getLon()));
		rect.getStyle().setColor(color);
		addMapRectangle(rect);
	}
	
	public Location getLocationMarker(int markerItem) {
		validateMarkerItem(markerItem);
		double latitude = getMapMarkerList().get(markerItem).getLat();
		double longitude = getMapMarkerList().get(markerItem).getLon();
		return new Location(latitude, longitude);
	}

	private void validateMarkerItem(int markerItem) {
		if (markerItem < 0 || markerItem > getMapMarkerList().size())
			throw new IllegalArgumentException("Indice fuera de rango");
	}
}
