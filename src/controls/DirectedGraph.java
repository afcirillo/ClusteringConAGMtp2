package controls;

import java.io.Serializable;
import java.util.ArrayList;

public class DirectedGraph <T> implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private boolean [][] edge;
	private double [][] edgeWeight;
	private int vertexCount;
	private ArrayList<T> info;

	public DirectedGraph(int vertexCount) {
		if (vertexCount <= 0)
			throw new IllegalArgumentException("La cantidad de vertices es:" + vertexCount);
		this.vertexCount = vertexCount;
		this.edgeWeight = new double[vertexCount][vertexCount];
		this.edge = new boolean[vertexCount][vertexCount];
	}
	
	public DirectedGraph(ArrayList<T> info) {
		if (info == null)
			throw new IllegalArgumentException("la informacion es nula");
		this.vertexCount = info.size();
		this.edgeWeight = new double[vertexCount][vertexCount];
		this.edge = new boolean[vertexCount][vertexCount];
		setInfo(info);
	}
	
	public void addEdge(int vertex1, int vertex2, double weight, boolean bidirectional) {
		validateLoop(vertex1, vertex2);
		validateVertex(vertex1);
		validateVertex(vertex2);
		
		this.edge[vertex1][vertex2] = true;
		this.edgeWeight[vertex1][vertex2] = weight;
		if (bidirectional) {
			this.edge[vertex2][vertex1] = true;
			this.edgeWeight[vertex2][vertex1] = weight;
		}
	}
	
	public void addEdge(int vertex1, int vertex2, double weight) {
		addEdge(vertex1, vertex2, weight, false);
	}
	
	public void addEdge(int vertex1, int vertex2) {
		addEdge(vertex1, vertex2, 0, false);
	}
	
	public void removeEdge(int vertex1, int vertex2, boolean bidirectional) {
		validateLoop(vertex1, vertex2);
		validateVertex(vertex1);
		validateVertex(vertex2);
		
		this.edge[vertex1][vertex2] = false;
		this.edgeWeight[vertex1][vertex2] = 0;
		if (bidirectional) {
			this.edge[vertex2][vertex1] = false;
			this.edgeWeight[vertex2][vertex1] = 0;
		}
	}
	
	public void removeEdge(int vertex1, int vertex2) {
		removeEdge(vertex1, vertex2, false);
	}
	
	public boolean edgeExists(int vertex1, int vertex2) {
		validateLoop(vertex1, vertex2);
		validateVertex(vertex1);
		validateVertex(vertex2);
		
		return this.edge[vertex1][vertex2];
	}
	
	public int degree(int vertex) {
		validateVertex(vertex);
		
		return neighbour(vertex).size();
	}

	public void setEdgeWeight(int vertex1, int vertex2, double weight) {
		if (!edgeExists(vertex1, vertex2)) {
			addEdge(vertex1, vertex2, weight);
			return;
		}
		this.edgeWeight[vertex1][vertex2] = weight;
	}
	
	public double getEdgeWeight(int vertex1, int vertex2) {
		if (!edgeExists(vertex1, vertex2))
			return 0;
		return this.edgeWeight[vertex1][vertex2];
	}
	
	public DirectedGraph<T> getMST() {
		
		return primMST();
	}
	
	private DirectedGraph<T> primMST() {
		int[] parent = new int[getVertexCount()];
		double[] key = new double[getVertexCount()];
		boolean[] mstSet = new boolean[getVertexCount()];
		
		for (int i = 0; i < key.length; i++) {
			key[i] = Double.MAX_VALUE;
		}
		
		key[0] = 0;
		parent[0] = 0;
		
		for (int c = 0; c < key.length-1; c++) {
			int i = minKey(key, mstSet);
			mstSet[i] = true;
			
			for (int j = 0; j < key.length; j++) {
				if (i == j)
					continue;
				if (edgeExists(i, j) && !mstSet[j] && getEdgeWeight(i, j) < key[j]) {
					parent[j] = i;
					key[j] = getEdgeWeight(i, j);
				}
			}
		}
		
		DirectedGraph<T> res = new DirectedGraph<T>(getVertexCount());
		for (int i = 1; i < key.length; i++) {
			res.addEdge(parent[i], i, key[i]);
		}
		
		return res;
	}
	
	private int minKey(double[] key, boolean[] mstSet) {
		double min = Double.MAX_VALUE;
		int minIndex = 0;
		
		for (int i = 0; i < key.length; i++) {
			if ( !mstSet[i] && key[i] < min) {
				min = key[i];
				minIndex = i;
			}
		}
		return minIndex;
	}
	
	
	public DirectedGraph<T> clone() {
		return this.clone();
	}
	
	public ArrayList<Integer> neighbour(int vertex){
		validateVertex(vertex);
		ArrayList<Integer> _neighbour = new ArrayList<Integer>();
		for (int i = 0; i < this.vertexCount; i++) {
			if (vertex == i)
				continue;
			if (edgeExists(vertex, i))
				_neighbour.add(i);
		}
		return _neighbour;
	}
	
	public ArrayList<Edge> edges(){
		ArrayList<Edge> res = new ArrayList<Edge>();

		for (int i = 0; i < getVertexCount(); i++) {
			for (int j = 0; j < getVertexCount(); j++) {
				if (i == j)
					continue;
				if (!edgeExists(i, j))
					continue;
				
				res.add(new Edge(i, j, getEdgeWeight(i, j)));
			}
		}
		return res;
	}
	
	private void validateVertex(int vertex) {
		if (vertex < 0 || vertex >= this.vertexCount)
			throw new IllegalArgumentException("El vertice no existe: " + vertex);
	}
	
	private void validateLoop(int vertex1, int vertex2) {
		if (vertex1 == vertex2)
			throw new IllegalArgumentException("El vertice1 no puede ser igual al vertice2");
	}
	
	public int getVertexCount() {
		return vertexCount;
	}

	public void setVertexCount(int vertexCount) {
		this.vertexCount = vertexCount;
	}
	
	public String toString() {
		String res = "";
		ArrayList<Edge> edges = edges();
		int item = 0;
		for (Edge edge: edges) {
			res += item + edge.toString() + "\n";
			item++;
		}
		return res;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<T> getInfo() {
		return (ArrayList<T>) info.clone();
	}

	@SuppressWarnings("unchecked")
	public void setInfo(ArrayList<T> info) {
		this.info = (ArrayList<T>) info.clone();
	}
	
}
