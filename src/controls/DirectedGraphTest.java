package controls;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

public class DirectedGraphTest {

	@Test
	public void addEdgeTest1() {
		DirectedGraph graph = new DirectedGraph(3);
		
		graph.addEdge(1, 2);
		
		assertTrue(graph.edgeExists(1, 2));
	}
	
	@Test
	public void addEdgeTest2() {
		DirectedGraph graph = new DirectedGraph(3);
		
		graph.addEdge(1, 2);
		
		assertTrue(graph.edgeExists(2, 1));
	}
	
	@Test
	public void removeEdgeTest1() {
		DirectedGraph graph = new DirectedGraph(3);
		
		graph.addEdge(2, 1);
		
		graph.removeEdge(2, 1);
		
		assertFalse(graph.edgeExists(2, 1));
	}
	
	@Test
	public void removeEdgeTest2() {
		DirectedGraph graph = new DirectedGraph(3);
		
		graph.addEdge(2, 1);
		
		graph.removeEdge(1, 2);
		
		assertFalse(graph.edgeExists(2, 1));
	}
	
	@Test
	public void vertexGradeTest1() {
		DirectedGraph graph = new DirectedGraph(4);
		
		graph.addEdge(0, 1);		
		graph.addEdge(0, 2);
		
		assertEquals(2, graph.degree(0));
	}
	
	@Test
	public void vertexGradeTest2() {
		DirectedGraph graph = new DirectedGraph(4);
		
		graph.addEdge(0, 1);		
		graph.addEdge(0, 2);
		
		assertEquals(1, graph.degree(1));
	}
	
	@Test
	public void vertexGradeTest3() {
		DirectedGraph graph = new DirectedGraph(4);
		
		graph.addEdge(0, 1);		
		graph.addEdge(0, 2);
		
		assertEquals(1, graph.degree(2));
	}
	
	@Test
	public void vertexGradeTest4() {
		DirectedGraph graph = new DirectedGraph(4);
		
		graph.addEdge(0, 1);		
		graph.addEdge(0, 2);
		
		assertEquals(0, graph.degree(3));
	}
	
	@Test
	public void edgeWeightTest1() {
		DirectedGraph graph = new DirectedGraph(4);
		
		graph.addEdge(0, 1);
		graph.setEdgeWeight(0, 1, 3);
		
		assertEquals(3, graph.getEdgeWeight(0, 1));
	}
	
	@Test
	public void edgeWeightTest2() {
		DirectedGraph graph = new DirectedGraph(4);
		
		graph.addEdge(0, 1);
		graph.setEdgeWeight(0, 1, 3);
		
		assertEquals(3, graph.getEdgeWeight(1, 0));
	}
	
	@Test
	public void negativeTest() {
		assertThrows(IllegalArgumentException.class, () -> new DirectedGraph(-1));
	}
	
	@Test
	public void negativeVertex1Test() {
		DirectedGraph graph = new DirectedGraph(3);
		assertThrows(IllegalArgumentException.class, () -> graph.addEdge(-1, 0));
	}
	
	@Test
	public void negativeVertex2Test() {
		DirectedGraph graph = new DirectedGraph(3);
		assertThrows(IllegalArgumentException.class, () -> graph.addEdge(0, -1));
	}
	
	@Test
	public void vertex1DoesntExistTest() {
		DirectedGraph graph = new DirectedGraph(3);
		assertThrows(IllegalArgumentException.class, () -> graph.addEdge(3, 0));
	}
	
	@Test
	public void vertex2DoesntExistTest() {
		DirectedGraph graph = new DirectedGraph(3);
		assertThrows(IllegalArgumentException.class, () -> graph.addEdge(0, 3));
	}
	
	@Test
	public void neighbourTest() {
		DirectedGraph graph = new DirectedGraph(4);
		graph.addEdge(0, 1);
		graph.addEdge(0, 2);
		ArrayList<Integer> neighbour = new ArrayList<Integer>();
		neighbour.add(1);
		neighbour.add(2);
		ArrayList<Integer> neighbour2 = graph.neighbour(0); 
		
		assertTrue(neighbour2.equals(neighbour));		
	}

}
