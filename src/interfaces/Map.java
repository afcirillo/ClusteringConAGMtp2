package interfaces;

import java.awt.FlowLayout;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JFrame;
import java.awt.SystemColor;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.event.WindowStateListener;
import java.awt.event.ActionEvent;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JFileChooser;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Label;
import java.awt.Point;

import controls.DirectedGraph;
import controls.Location;
import controls.MapViewer;

public class Map extends JFrame {
	enum Actions {
		NONE, ADD_POINTS, REMOVE_POINTS, MOVE_POINTS, REMOVING_POINTS, MOVING_POINT
	};

	private final static String APP_NAME = "CMZMapViewer";
	private static final int MAX_ADJUSTMENT = 20;
	DirectedGraph<Location> graph;
	ArrayList<Location> locationList;
	private MapViewer map;
	private JMenuBar menuBar;
	private JMenu mnuFile;
	private JMenuItem mniOpen;
	private JMenuItem mniSave;
	private JMenuItem mniExportSolution;
	private JMenuItem mniImportSolution;
	private JMenuItem mniExit;
	private JMenu mnuView;
	private JCheckBoxMenuItem mniMST;
	private JCheckBoxMenuItem mniClusters;
	private JMenu mnuHelp;
	private JMenuItem mniAbout;
	private JSlider sldClusterAdjustment;
	private Label lblClustersAdjustment;
	private JMenu mnuEdit;
	private JMenuItem mniClearMap;
	private JMenuItem mniAddPoints;
	private JMenuItem mniRemovePoints;
	private Actions action;
	private JMenuItem mniSaveAs;
	private String openedFile;
	private boolean fileWasEdited;
	private JMenuItem mniMovePoints;
	private int[] selectedPointsItems;
	private int selectedPointItem;
	private Point mouseInitialPoint;

	public Map() {
		super(APP_NAME);
		
		openedFile = "";
		getContentPane().setBackground(SystemColor.activeCaption);
		getContentPane().setLayout(new BorderLayout(0, 0));

		map = new MapViewer();
		map.setZoomControlsVisible(false);
		map.setScrollWrapEnabled(true);
		getContentPane().add(map, BorderLayout.CENTER);
		map.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		locationList = new ArrayList<Location>();
		setAction(Actions.NONE);

		lblClustersAdjustment = new Label("Ajustar clusters");
		lblClustersAdjustment.setVisible(false);
		map.add(lblClustersAdjustment);

		sldClusterAdjustment = new JSlider();
		sldClusterAdjustment.setMaximum(15);
		sldClusterAdjustment.setMinimum(5);
		sldClusterAdjustment.setValue(10);
		sldClusterAdjustment.setVisible(false);
		sldClusterAdjustment.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				showClusterFunction();
			}
		});

		map.add(sldClusterAdjustment);

		menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		mnuFile = new JMenu("Archivo");
		menuBar.add(mnuFile);

		mniOpen = new JMenuItem("Abrir");
		mniOpen.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				openInstanceFile();
			}
		});
		mnuFile.add(mniOpen);

		mniSave = new JMenuItem("Guardar");
		mniSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				saveInstanceFile(openedFile);
			}
		});
		mnuFile.add(mniSave);

		mniSaveAs = new JMenuItem("Guardar como...");
		mniSaveAs.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				saveInstanceFile("");
			}
		});
		mnuFile.add(mniSaveAs);

		mniImportSolution = new JMenuItem("Importar solucion");
		mniImportSolution.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				importSolutionFile();
			}
		});

		mnuFile.add(mniImportSolution);

		mniExportSolution = new JMenuItem("Exportar solucion");
		mniExportSolution.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				exportSolutionFile();
			}
		});
		mnuFile.add(mniExportSolution);

		mniExit = new JMenuItem("Salir");
		mniExit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (validateOnClosing())
					System.exit(0);
			}
		});
		mnuFile.add(mniExit);

		mnuEdit = new JMenu("Edicion");
		menuBar.add(mnuEdit);

		mniAddPoints = new JMenuItem("Agregar puntos");
		mnuEdit.add(mniAddPoints);
		mniAddPoints.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setAction(Actions.ADD_POINTS);
				updateCursor();
			}
		});

		mniRemovePoints = new JMenuItem("Eliminar puntos");
		mniRemovePoints.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setAction(Actions.REMOVE_POINTS);
				updateCursor();
			}
		});
		
		mniMovePoints = new JMenuItem("Mover puntos");
		mniMovePoints.setEnabled(false);
		mniMovePoints.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setAction(Actions.MOVE_POINTS);
				updateCursor();
			}
		});
		mnuEdit.add(mniMovePoints);

		mnuEdit.add(mniRemovePoints);

		mniClearMap = new JMenuItem("Limpiar mapa");
		mniClearMap.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				clearMap();
			}
		});
		mnuEdit.add(mniClearMap);

		mnuView = new JMenu("Ver");
		menuBar.add(mnuView);

		mniMST = new JCheckBoxMenuItem("Arbol Generador Minimo");
		mniMST.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				showMSTfunction();
			}
		});
		mnuView.add(mniMST);

		mniClusters = new JCheckBoxMenuItem("Clusters");
		mniClusters.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				showClusterFunction();
			}
		});
		mnuView.add(mniClusters);

		mnuHelp = new JMenu("Ayuda");
		menuBar.add(mnuHelp);

		mniAbout = new JMenuItem("Acerca de...");
		mnuHelp.add(mniAbout);

		validateMenu();

		map.addMouseListener(new MouseListener() {

			@Override
			public void mousePressed(MouseEvent arg0) {
				mouseInitialPoint = arg0.getPoint();
				Actions action = getAction();
				switch (action) {
				case ADD_POINTS:
					if (arg0.getButton() == MouseEvent.BUTTON1) {
						addOwnPoint(arg0.getPoint());
					}
					break;
				case REMOVE_POINTS:
					if (arg0.getButton() == MouseEvent.BUTTON1) {
						setAction(Actions.REMOVING_POINTS);
						selectedPointItem = map.getItemMarkerAt(arg0.getPoint());
						if (selectedPointItem < 0)
							return;
						removePoint(selectedPointItem);
					}
					break;
				case MOVE_POINTS:
					if (arg0.getButton() == MouseEvent.BUTTON1) {
						selectedPointItem = map.getItemMarkerAt(arg0.getPoint());
						if (selectedPointItem < 0)
							return;	
						setAction(Actions.MOVING_POINT);
					}
					break;
				}
			}

			@Override public void mouseClicked(MouseEvent arg0) {}
			@Override public void mouseEntered(MouseEvent arg0) {updateCursor();}
			@Override public void mouseExited(MouseEvent arg0) {setCursor(Cursor.DEFAULT_CURSOR);}
			@Override 
			public void mouseReleased(MouseEvent arg0) {
				Actions action = getAction();
				switch (action) {
				case MOVING_POINT:
					setAction(Actions.MOVE_POINTS);
					if (selectedPointItem < 0 || selectedPointItem >= locationList.size())
						return;
					locationList.get(selectedPointItem).setLocation(map.getLocationMarker(selectedPointItem));
					updateMap();
					selectedPointItem = -1;
					break;
				
				case REMOVING_POINTS:
					setAction(Actions.REMOVE_POINTS);
					map.removeAllMapRectangles();
					if (selectedPointsItems == null || selectedPointsItems.length == 0)
						return;
					
					for (int index: selectedPointsItems)
						removePoint(index);
					selectedPointsItems = null;
					break;
				}
			}
		});

		map.addMouseMotionListener(new MouseMotionListener() {
			@Override
			public void mouseMoved(MouseEvent e) {
				Actions action = getAction();
				switch (action) {
				case REMOVE_POINTS:
					highlightPointAt(e.getPoint(), Color.RED);
					break;
				case REMOVING_POINTS:
					highlightPointsIntoArea(mouseInitialPoint, e.getPoint(), Color.RED);
					break;
				case MOVE_POINTS:
					highlightPointAt(e.getPoint(), Color.GREEN);
					break;
				case MOVING_POINT:
					movingPoint(selectedPointItem, e.getPoint());
					break;
				}
			}

			@Override
			public void mouseDragged(MouseEvent e) {
				Actions action = getAction();
				switch (action) {
				case MOVING_POINT:
					movingPoint(selectedPointItem, e.getPoint());
					break;
				case REMOVING_POINTS:
					map.removeAllMapRectangles();
					map.drawRectangle(mouseInitialPoint, e.getPoint(), Color.RED);
					highlightPointsIntoArea(mouseInitialPoint, e.getPoint(), Color.RED);
					break;
				}
			}
		});
		addWindowListener(new WindowListener() {			
			@Override
			public void windowClosing(WindowEvent e) {
				if (!validateOnClosing()) {
					setVisible(true);
					return;
				}
				System.exit(0);
			}
			@Override public void windowOpened(WindowEvent e) {}			
			@Override public void windowIconified(WindowEvent e) {}			
			@Override public void windowDeiconified(WindowEvent e) {}
			@Override public void windowDeactivated(WindowEvent e) {}			
			@Override public void windowClosed(WindowEvent e) {}			
			@Override public void windowActivated(WindowEvent e) {}
		});
		
		addKeyListener(new KeyListener() {
			@Override 
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					setAction(Actions.NONE);
					setCursor(Cursor.DEFAULT_CURSOR);
				}	
			}
			@Override public void keyTyped(KeyEvent e) {}			
			@Override public void keyReleased(KeyEvent e) {}			
		});

		initialize();
	}

	protected boolean validateOnClosing() {
		int answer = JOptionPane.showConfirmDialog(getParent(), "�Desea salir?", "Salir",
				JOptionPane.YES_NO_OPTION);
		if (answer != JOptionPane.YES_OPTION)
			return false;
		if (fileWasEdited) {
			answer = JOptionPane.showConfirmDialog(getParent(), "�Desea guardar los cambios?");
			if (answer == JOptionPane.CANCEL_OPTION)
				return false;
			if (answer == JOptionPane.YES_OPTION)
				saveInstanceFile(openedFile);
		}
		return true;
	}

	protected void highlightPointsIntoArea(Point p1, Point p2, Color color) {
		selectedPointsItems = map.getItemsMarkerFromSelection(p1, p2);
		map.unHighlightAllMarkers();
		if (selectedPointsItems.length == 0) 
			return;
		map.highlightMarkers(selectedPointsItems, color);
	}

	protected void highlightPointAt(Point point, Color color) {
		selectedPointItem = map.getItemMarkerAt(point);
		map.unHighlightAllMarkers();
		if (selectedPointItem < 0) 
			return;
		map.highlightMarker(selectedPointItem, color);
	}

	private void initialize() {
		setIconImage(new ImageIcon("image/logo.png").getImage());
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setSize(1024, 768);
		setMinimumSize(new Dimension(800, 600));
		setMaximumSize(Toolkit.getDefaultToolkit().getScreenSize());
		setExtendedState(JFrame.NORMAL | JFrame.MAXIMIZED_BOTH);
		setVisible(true);
	}

	protected void saveInstanceFile(String fileName) {
		if (locationList.isEmpty())
			return;
		if (fileName.isEmpty())
			fileName = selectFile("Guardar como", "Guardar", "Archivo de texto", "txt");
		if (fileName.isEmpty())
			return;

		if (fileName.indexOf(".txt") <= 0)
			fileName = fileName + ".txt";

		PrintWriter out;
		try {
			out = new PrintWriter(fileName);

			for (Location location : locationList) {
				out.print(location.getLatitude());
				out.println(" " + location.getLongitude());
			}
			out.close();
		} catch (FileNotFoundException e) {
			System.err.println("File doesn't exist");
			e.printStackTrace();
		}
		super.setTitle(APP_NAME + " - " + fileName);
		fileWasEdited = false;
	}

	protected void removePoint(int pointItem) {
		if (pointItem < 0 || pointItem >= map.getMapMarkerList().size())
			return;
		locationList.remove(pointItem);
		map.removeMapMarker(map.getMapMarkerList().get(pointItem));

		validateMenu();
		if (locationList.size() > 0) {
			updateMap();
		}
		fileWasEdited = true;

	}

	protected void addOwnPoint(Point point) {
		Location newMarker = new Location(map.getPosition(point).getLat(), map.getPosition(point).getLon());
		addLocation(newMarker);
		updateMap();
		fileWasEdited = true;
	}
	
	protected void movingPoint(int selectedPointItem,Point newPosition) {
		map.unHighlightAllMarkers();
		if (selectedPointItem < 0)						
			return;
		map.highlightMarker(selectedPointItem, Color.GREEN);
		map.setMarkerPosition(selectedPointItem, newPosition);
		locationList.get(selectedPointItem).setLocation(new Location(map.getPosition(newPosition).getLat(), map.getPosition(newPosition).getLon()));
		fileWasEdited = true;
		updateMap();
	}

	@SuppressWarnings({ "unchecked", "resource" })
	protected void importSolutionFile() {
		String file = selectFile("Importar solucion", "Importar", "Archivo de Solucion de Clusters", "asc");
		if (file.isEmpty())
			return;
		super.setTitle(APP_NAME);
		openedFile = "";
		mniClusters.setSelected(true);
		clearMap();
		FileInputStream fis;
		ObjectInputStream in;

		try {
			fis = new FileInputStream(file);
			in = new ObjectInputStream(fis);
			graph = (DirectedGraph<Location>) in.readObject();
			in.close();

			locationList = graph.getInfo();
			map.addPoints(locationList);
			map.showClusters(graph);
		} catch (FileNotFoundException e) {return;
		} catch (IOException e) {return;
		} catch (ClassNotFoundException e) {return;}
		mniClusters.setSelected(true);
		validateMenu();
	}

	private void exportSolutionFile() {
		String file = selectFile("Exportar solucion", "Exportar", "Archivo de Solucion de CLusters", "asc");
		if (file.isEmpty())
			return;
		graph.setInfo(locationList);
		FileOutputStream fos;
		ObjectOutputStream out;
		
		if (file.indexOf(".asc") <= 0)
			file = file + ".asc";
		try {
			fos = new FileOutputStream(file);
			out = new ObjectOutputStream(fos);
			out.writeObject(graph);
			out.close();
		} catch (FileNotFoundException e) {e.printStackTrace();
		} catch (IOException e) {e.printStackTrace();}
		validateMenu();
	}

	private void openInstanceFile() {
		openedFile = selectFile("Abrir", "Abrir", "Archivo de texto", "txt");
		if (openedFile.isEmpty())
			return;
		super.setTitle(APP_NAME + " - " + openedFile);
		clearMap();
		locationList = importCoordinates(openedFile);
		map.addPoints(locationList);
		updateMap();
	}

	private String selectFile(String title, String buttonText, String type, String extension) {
		JFileChooser chooser = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter(type, extension);
		chooser.setFileFilter(filter);
		chooser.setDialogTitle(title);
		chooser.setApproveButtonText(buttonText);
		chooser.setCurrentDirectory(new File(""));

		if (chooser.showOpenDialog(null) == JFileChooser.CANCEL_OPTION)
			return "";
		return chooser.getSelectedFile().getAbsolutePath();	
	}

	private void updateMap() {
		showMSTfunction();
		showClusterFunction();
	}

	private void showMSTfunction() {
		map.removeAllMapPolygons();
		if (!mniMST.isSelected())
			return;
		generateGraph();
		graph = graph.getMST();
		map.setGraph(graph);
		validateMenu();
	}

	private void showClusterFunction() {
		sldClusterAdjustment.setVisible(mniClusters.isSelected());
		lblClustersAdjustment.setVisible(mniClusters.isSelected());
		validateMenu();
		if (mniClusters.isSelected()) {
			generateGraph();
			graph = graph.getMST();
			double adjustment = ((double) (MAX_ADJUSTMENT - sldClusterAdjustment.getValue())) / 10.0;
			map.findClusters(graph, adjustment);
			map.showClusters(graph);
			return;
		}
		map.hideClusters();
	}

	public static ArrayList<Location> importCoordinates(String file) {
		ArrayList<Location> locations = new ArrayList<Location>();
		try {
			FileInputStream fis = new FileInputStream(file);
			Scanner scan = new Scanner(fis);
			while (scan.hasNext()) {
				locations.add(new Location(Double.parseDouble(scan.next()), Double.parseDouble(scan.next())));
			}
		} catch (Exception e) {
		}
		return locations;
	}

	private void generateGraph() {
		graph = new DirectedGraph<Location>(locationList.size());
		double distance = 0;
		double x1, x2, y1, y2;
		for (int i = 0; i < locationList.size(); i++) {
			for (int j = 0; j < locationList.size(); j++) {
				if (i == j)
					continue;
				x1 = locationList.get(i).getLongitude();
				y1 = locationList.get(i).getLatitude();
				x2 = locationList.get(j).getLongitude();
				y2 = locationList.get(j).getLatitude();
				distance = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));

				graph.addEdge(i, j, distance);
			}
		}

	}

	private void addLocation(Location location) {
		locationList.add(location);
		map.drawMarker("", location);
	}

	private void clearMap() {
		graph = null;
		map.removeAllMapMarkers();
		map.removeAllMapPolygons();
		locationList.clear();
		sldClusterAdjustment.setVisible(false);
		mniMST.setSelected(false);
		mniClusters.setSelected(false);
		validateMenu();
	}

	public Actions getAction() {
		return action;
	}

	public void setAction(Actions action) {
		this.action = action;
	}
	
	public void updateCursor() {
		Actions action = getAction();
		String imagePath = "";
		switch(action) {
		case ADD_POINTS:
			imagePath = "image/add_location.png";
			break;
		case MOVE_POINTS:
			imagePath = "image/move_location.png";
			break;
		case REMOVE_POINTS:
			imagePath = "image/del_location.png";
			break;
		case NONE:
			setCursor(Cursor.DEFAULT_CURSOR);
			return;
		}
		setCursor(Toolkit.getDefaultToolkit().createCustomCursor(new  ImageIcon(imagePath).getImage(),new Point(15,31),""));
	}
	private void validateMenu() {
		mniSave.setEnabled(!(locationList.isEmpty()));
		mniSaveAs.setEnabled(!(locationList.isEmpty()));
		mniExportSolution.setEnabled(mniClusters.isSelected());
		mniClearMap.setEnabled(!(locationList.isEmpty()));
        mniRemovePoints.setEnabled(!(locationList.isEmpty()));
        mniMovePoints.setEnabled(!(locationList.isEmpty()));
        mniMST.setEnabled(!(locationList.isEmpty()));
        mniClusters.setEnabled(!(locationList.isEmpty()));
	}
}